var expect = require('chai').expect;
var $ = require('chai-jquery');
var chhttp = require('chai-http');
var request =require('request');

describe("Pruebas sencillas", function() {
it('Test suma', function() {
  expect(9+4).to.equal(13);
  });
});

describe("Pruebas red", function() {
it('Test internet', function(done) {
  request.get("http://www.amazon.es",
              function(error, response, body) {
                expect(response.statusCode).to.equal(200);
                done();
  });
});

it('Test internet 2', function(done) {
  request.get("http://localhost:8082",
              function(error, response, body) {
                expect(response.statusCode).to.equal(200);
                done();
  });
});

/*it('Test header', function(done) {
  request.get("http://localhost:8082",
              function(res ) {
                expect(res).to.have.header('Bienenido a mi blog');
                done();
  });
});*/
});

describe("Test contenido HTML", function() {
  it('Test H1', function() {
    request.get("http://localhost:8082",
      function(error, res, body) {
        expect($('body h1')).to.have.text("Bienvenido a mi blog");
        done();
      });
  });
});


/* $() identifica elementos
#identificador = <xxx id="identificador"...
elemento = <elemento....
.clase = <xxx class="clase"...

ej
para buscar <table id="prueba" class="clase1"....
ponemos $("table#prueba.clase1") */
