'use strict';

const express = require('express');
const path = require('path');

// Constantes
const PORT = 3000;

//App
const app = express();

app.use(express.static(__dirname));

app.get('/', function(req, res) {
  //res.send("Bienvenido terricola\n");
  res.sendFile(path.join(__dirname+'/index.html'));
});

app.listen(PORT);
console.log("Express funcionando en el puerto" + PORT);
